﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

    public Text quitText;
    public Text restartText;

    public Button restart;
    public Button quit;

	// Use this for initialization
	void Start () {
        restart = restart.GetComponent<Button>();
        quit = quit.GetComponent<Button>();
	}

    public void OnQuitClicked()
    {
        Application.Quit();
    }

    public void OnRestartClicked()
    {
        Initiate.Fade("MainGame", Color.black, 0.9f);
    }

    private void Update()
    {
        //
    }
}
