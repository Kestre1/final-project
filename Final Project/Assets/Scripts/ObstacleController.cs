﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

    public float scrollSpeed = 5.0f;
    public GameObject[] obstacles;
    public GameObject[] powerups;
    public float frequency = 0.5f; // obstacles per second
    public Transform obstacleSpawnPoint;

    private float counter = 0.0f;
    private bool isGameOver = false;

	// Use this for initialization
	void Start () {
        GenerateRandomObstacle();
	}
	
	// Update is called once per frame
	void Update () {

        if (isGameOver) return;

        // Generate Obstacles
        if (counter <= 0.0f)
        {
            if (Random.Range(0, 20) == 0)
            {
                GenerateRandomPowerup();
            }
            else
            {
                GenerateRandomObstacle();
            }
        }
        else
        {
            counter -= Time.deltaTime * frequency;
        }

        GameObject currentChild;

		// Scrolling
        for (int i = 0; i < transform.childCount; i++)
        {
            currentChild = transform.GetChild(i).gameObject;
            ScrollObstacle(currentChild);

            if(currentChild.transform.position.x < -20.0f)
            {
                Destroy(currentChild);
            }
        }
	}

    void ScrollObstacle(GameObject currentObstacle)
    {
        currentObstacle.transform.position -= Vector3.right * (scrollSpeed * Time.deltaTime);

    }

    void GenerateRandomObstacle()
    {
        GameObject newObstacle = Instantiate(obstacles[Random.Range(0, obstacles.Length)], obstacleSpawnPoint.position, Quaternion.identity) as GameObject;
        newObstacle.transform.parent = transform;
        counter = 1.0f;
    }

    void GenerateRandomPowerup()
    {
        GameObject newPowerup = Instantiate(powerups[Random.Range(0, powerups.Length)], obstacleSpawnPoint.position, Quaternion.identity) as GameObject;
        newPowerup.transform.parent = transform;
        counter = 1.0f;
    }

    public void RemoveFirstObstacle()
    {
        GameObject firstObstacle = transform.GetChild(0).gameObject;
        //Debug.Log(firstObstacle);
        firstObstacle.SetActive(false);
    }

    public void ClearObstacles()
    {
        var children = new List<GameObject>();
        foreach (Transform child in transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

    public void GameOver()
    {
        isGameOver = true;
    }
}
