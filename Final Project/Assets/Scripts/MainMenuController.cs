﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    public Text startText;
    public Text controlsText;
    public Text quitText;
    public Text quitConfirm;
    public Text yes;
    public Text no;

    public GameObject defaultMenu;
    public GameObject quitMenu;
    public GameObject controlsMenu;

    public Button startButton;
    public Button controlsButton;
    public Button quitButton;
    public Button yesButton;
    public Button noButton;
    public Button returnButton;

    // Use this for initialization
    void Start ()
    {
        startButton = startButton.GetComponent<Button>();
        controlsButton = controlsButton.GetComponent<Button>();
        quitButton = quitButton.GetComponent<Button>();
        yesButton = yesButton.GetComponent<Button>();
        noButton = noButton.GetComponent<Button>();
        returnButton = returnButton.GetComponent<Button>();

        // quitConfirm.enabled = false;
        // yes.enabled = false;
        // no.enabled = false;

        controlsMenu.SetActive(false);
        quitMenu.SetActive(false);
    }

    public void OnQuitClicked()
    {
        Debug.Log("Quit clicked");
        // controlsText.enabled = false;
        // startText.enabled = false;
        // quitText.enabled = false;
        defaultMenu.SetActive(false);
        quitMenu.SetActive(true);
       // quitConfirm.enabled = true;
       // yes.enabled = true;
       // no.enabled = true;
    }
    public void OnNoClicked()
    {
        Debug.Log("No clicked");
        // controlsText.enabled = true;
        // startText.enabled = true;
        // quitText.enabled = true;
        quitMenu.SetActive(false);
        defaultMenu.SetActive(true);
       // quitConfirm.enabled = false;
       // yes.enabled = false;
       // no.enabled = false;
    }

    public void StartGame()
    {
        Initiate.Fade("MainGame", Color.black, 0.9f);
    }

    public void ShowControls()
    {
        defaultMenu.SetActive(false);
        quitMenu.SetActive(false);
        controlsMenu.SetActive(true);
    }

    public void ReturnToMain()
    {
        defaultMenu.SetActive(true);
        quitMenu.SetActive(false);
        controlsMenu.SetActive(false);
    }

    public void CallQuitGame()
    {
        Debug.Log("Confirm");
        Application.Quit();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
