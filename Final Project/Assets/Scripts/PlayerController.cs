﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    
    public int playerStamina;
    public int playerCoins;

    public int coinCounter;

    public float jumpPower = 10.0f;
    
    public Text coinText;
    public Image staminaImg;

    public Text powerupText;
    public Image powerupImage;
    
    public AudioClip shootSound;
    public AudioClip jumpSound;
    public AudioClip coinSound;
    public AudioClip deathSound;

    public ObstacleController obstacleController;

    private Animator animator;
    
    Rigidbody2D playerBody;

    public Image guardianImage;

    private float posX = 0.0f;

    private Vector3 rootPosition;

    private bool isGrounded = false;
    private bool isGameOver = false;
    private bool hasGuardian = false;


	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        playerBody = transform.GetComponent<Rigidbody2D>();
        posX = transform.position.x;
        obstacleController = GameObject.FindObjectOfType<ObstacleController>();

        coinCounter = 0;

        coinText.text = "Score: " + playerCoins;

        rootPosition = transform.position;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (Input.GetKey(KeyCode.Space) && isGrounded && !isGameOver)
        {
            playerBody.AddForce(Vector3.up * (jumpPower * playerBody.mass * playerBody.gravityScale * 20.0f));
            animator.SetTrigger("Jump");
        }

        if(coinCounter == 5)
        {
            obstacleController.scrollSpeed += 0.5f;
            //Debug.Log(obstacleController.scrollSpeed);
            playerBody.gravityScale += 0.25f;
            coinCounter = 0;
        }

        //Lateral collision check
        if (transform.position.x < posX)
        {
            if (hasGuardian)
            {
                //transform.position = new Vector3(-6, 8, 0);
                obstacleController.RemoveFirstObstacle();
                //Wait(1);
                hasGuardian = false;
                transform.position = new Vector3(posX, -2.15f, 0);
            }
            else
            {
                GameOver();
            }
        }

	}

    void Update()
    {
        //Debug.Log(playerBody.velocity);
    }

    void GameOver()
    {
        isGameOver = true;
        animator.enabled = false; // stop player animation
        obstacleController.GameOver();

        Initiate.Fade("GameOver", Color.black, 0.9f);
    }

    private void OnCollisionEnter2D(Collision2D other) // touches ground
    {
        if(other.collider.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    private void OnCollisionStay2D(Collision2D other) // stays on ground
    {
        if (other.collider.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other) // leaves ground
    {
        if (other.collider.tag == "Ground")
        {
            isGrounded = false;
            animator.ResetTrigger("Jump");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Coin")
        {
            SoundManager.instance.PlaySingle(coinSound);
            playerCoins++;
            coinCounter++;
            coinText.text = "Score: " + playerCoins;
            other.gameObject.SetActive(false);
        }
        if(other.tag == "Hazard")
        {
            if (hasGuardian)
            {
                transform.position = new Vector3(-6, 8, 0);
                hasGuardian = false;
            }
            else
            {
                GameOver();
            }
        }
        if(other.tag == "Guardian")
        {
            hasGuardian = true;
            other.gameObject.SetActive(false);
            //StartCoroutine(Wait(3));
            //powerupText.text = "";
        }
        if(other.tag == "CoinPowerup")
        {
            playerCoins += 5;
            coinText.text = "Score: " + playerCoins;
            //Wait(3);
            //powerupText.text = "";
            other.gameObject.SetActive(false);
        }
        if(other.tag == "Stopwatch")
        {
            obstacleController.scrollSpeed -= 2;
            //Wait(3);
            other.gameObject.SetActive(false);
        }
    }

    IEnumerator Wait(int time)
    {
        yield return new WaitForSeconds(time);
    }

}
